(function (angular) {
    'use strict';
    angular.module('core').service('requestService', requestService);

    //requestService.$inject = ['atletaCursaFactory','$filter'];
    function requestService() {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);
            object.saveFields = ['subject','comments','fullRequest','coachComment','coachRequest'];
            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            object.attributes.createdAt = moment(object.parseObject.createdAt).format('DD-MM-YYYY');

            object.formatEndDate = moment(object.endDate).format('DD-MM-YYYY');
            object.formatOpenDate = moment(object.parseObject.createdAt).format('DD-MM-YYYY');
            object.attributes.printDateRange =
                moment(object.startDate).format('DD-MM-YYYY') + ' - ' +
                moment(object.endDate).format('DD-MM-YYYY')

            if ( parseObject.get('user') ){
                object.userId = parseObject.get('user').id;
                object.attributes.user = parseObject.get('user')
            }
            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            object.getCreatedAt = function () {
                return this.parseObject.createdAt;
            };
            object.full = function (data, isCoach) {
                data['fullRequest'] = true;
                if ( isCoach ){
                    data['coachRequest'] = true;
                }
                object.update(data)
            }
            object.update = function (data, callback) {
                var i;
                for (i in data) {
                    if ( object.saveFields.indexOf(i)==-1){
                        continue;
                    }
                    this.parseObject.set(i, data[i]);
                }
                this.parseObject.save(null, {
                    success: function (answer) {
                        if (callback)
                            callback({success: true});
                    },
                    error: function (answer, error) {
                        if (callback)
                            callback({success: false, error: error.message});
                    }
                })
            };

            object.delete = function (callback) {
                this.parseObject.destroy({
                    success: function (myObject) {
                        callback({success:true})
                    },
                    error: function (myObject, error) {
                        callback({success:false,error:error})
                    }
                });
            };

            object.saveAnswer = function (answer, callback) {
                this.parseObject.set('coachComment', answer);
                this.parseObject.save(null, {
                    success: function (answer) {
                        if (callback)
                            callback({success: true});
                    },
                    error: function (answer, error) {
                        if (callback)
                            callback({success: false, error: error.message});
                    }
                })
            };

            object.close = function (callback) {
                this.parseObject.set('closed', true);
                this.parseObject.set('endDate', moment().toDate());
                this.parseObject.save(null, {
                    success: function (answer) {
                        if (callback)
                            callback({success: true});
                    },
                    error: function (answer, error) {
                        if (callback)
                            callback({success: false, error: error.message});
                    }
                })
            };

            object.open = function (callback) {
                this.parseObject.set('closed', false);
                this.parseObject.set('endDate', null);

                this.parseObject.save(null, {
                    success: function (answer) {
                        if (callback)
                            callback({success: true});
                    },
                    error: function (answer, error) {
                        if (callback)
                            callback({success: false, error: error.message});
                    }
                })
            };

            return object;

        }
    }
})(angular);

