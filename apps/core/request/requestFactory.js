(function (angular) {
    'use strict';
    angular.module('core').service('requestFactory', requestFactory);

    requestFactory.$inject = ['requestService'];

    function requestFactory(requestService) {

        return {
            getById: getById,
            getByUser: getByUser,
            getWeeklyRequestByUser: getWeeklyRequestByUser,
            getRequestWitOutCoachComment: getRequestWitOutCoachComment,
            store: store,
        };



        function store(data, callback) {
            var parseObject = Parse.Object.extend("Request");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }
            store.save(null, {
                success: function (obj) {
                    callback({success: true});
                },
                error: function (obj, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getById(id, callback) {
            var parseObject = Parse.Object.extend("Request");
            var query = new Parse.Query(parseObject);
            query.include('user');

            query.get(id, {
                success: function (results) {
                    callback(requestService(results))
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getRequestWitOutCoachComment(callback) {
            var date = moment().subtract(10,'days').toDate();

            var parseObject = Parse.Object.extend("Request");


            var closed = new Parse.Query(parseObject);
            closed.equalTo('closed', false);
            closed.greaterThan('endDate', date);


            var opened = new Parse.Query(parseObject);
            opened.notEqualTo('closed', true);
            //opened.greaterThan('endDate', date);

            var mainQuery = Parse.Query.or(closed, opened);
            mainQuery.descending('startDate');
            mainQuery.include('user');

            mainQuery.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(requestService(row));
                    });
                    callback(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getByUser(user, callback) {
            var parseObject = Parse.Object.extend("Request");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);
            query.include('user');
            query.descending('startDate');
            query.limit(3);
            query.find({
                success: function (results) {
                    var rows = [];
                    angular.forEach(results, function (row) {
                        rows.push(requestService(row));
                    });
                    callback(rows);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function getWeeklyRequestByUser(user, callback) {

            var now = moment().utc().toDate();
            var parseObject = Parse.Object.extend("Request");
            var query = new Parse.Query(parseObject);
            query.equalTo('user', user);
            query.lessThanOrEqualTo('startDate', now);
            query.greaterThanOrEqualTo('endDate', now);
            query.equalTo('coachRequest', true);
            query.include('user');
            query.descending('startDate');
            query.limit(3);
            query.find({
                success: function (result) {
                    if (result.length>0 ){
                        callback(requestService(result[0]));
                    }
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

    }
})(angular);

