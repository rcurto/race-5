(function (angular) {
    'use strict';
    angular.module('core').service('userService', userService);

    userService.$inject = ['atletaCursaFactory', 'cursesFactory', 'feedbackFactory', 'eventFactory', 'atletaMesocicleFactory', 'requestFactory'];
    function userService(atletaCursaFactory, cursesFactory, feedbackFactory, eventFactory, atletaMesocicleFactory, requestFactory) {
        return function (parseObject) {
            var userObject = angular.copy(parseObject.attributes);
            userObject.parseObject = angular.copy(parseObject);
            userObject.attributes = angular.copy(parseObject.attributes);
            userObject.joinCourses = [];
            userObject.attributes.raceHistory = parseObject.get('raceHistory') == undefined ? [] : parseObject.get('raceHistory');
            userObject.FullName = userObject.firstName+ ' '+ userObject.lastName;
            userObject.getRole = function () {
                var role = userObject.parseObject.get('role');
                if (role != undefined) {
                    return role.get('name');
                }
                return false;
            };
            userObject.isAthlete = function () {
                var role = userObject.getRole();
                if (role == 'athlete') {
                    return true;
                }
                return false;
            };
            userObject.isCoach = function () {
                var role = userObject.getRole();
                if (role == 'coach') {
                    return true;
                }
                return false;
            };

            userObject.isBasicPlan = function(){
                if (userObject.get('trainingPlan')=='basic'){
                    return true;
                }
                return false;
            };
            userObject.isPremiumPlan = function(){
                if (userObject.get('trainingPlan')=='premium'){
                    return true;
                }
                return false;
            };

            userObject.delete = function () {
                Parse.Cloud.run('userDelete', {id: userObject.getId()}, {
                    success: function (ratings) {
                    },
                    error: function (error) {
                    }
                });
            };
            userObject.update = function (data, callback) {

                var exep = ['createdAt', 'role', 'sessionToken', 'updatedAt'];
                for (var i in data) {
                    if (exep.indexOf(i) == -1) {
                        userObject.parseObject.set(i, data[i]);
                    }
                }
                if (data.raceHistory != undefined && data.raceHistory.length > 0) {
                    for (var i in data.raceHistory) {
                        delete data.raceHistory[i]['$$hashKey'];
                    }
                }

                userObject.parseObject.save(null, {
                    success: function (user) {
                        callback({success: true});
                    },
                    error: function (user, error) {
                        callback({success: false, error: error.message});
                    }
                });
                /*
                 Parse.Cloud.run('userEdit', {id: userObject.getId(), data: data }, {
                 success: function(user) {
                 callback({success:true})
                 },
                 error: function(error) {
                 callback({success:false, error:error.message})
                 }
                 });*/
            };

            userObject.get = function (key) {
                return this.attributes[key];
            };


            userObject.getId = function () {
                return this.parseObject.id;
            };
            userObject.getCreatedAt = function () {
                return this.parseObject.createdAt;
            };

            userObject.joinCourse = function (race) {


                userObject.joinCourses.push(race.id);
                eventFactory.joinRace(race, userObject.parseObject)
            };


            userObject.getJoinCoursesIds = function (callback) {
                var promise = new Parse.Promise();

                atletaCursaFactory.getByAthlete(userObject, function (rows) {
                    var ids = [];
                    angular.forEach(rows, function (row) {
                        if (row.get('nomcursa'))
                            ids.push(row.get('nomcursa').id);
                    });
                    userObject.joinCourses = ids;
                    if (callback){
                        callback(ids);
                    }
                    promise.resolve(ids);
                });
                return promise;
            };

            userObject.getNextRace = function () {
                return atletaCursaFactory.getNextRace(userObject.parseObject)
            };
            userObject.getAfterNextRace = function (callback) {
                atletaCursaFactory.getAfterNextRace(userObject.parseObject, function (result) {
                    callback(result)
                })
            };


            userObject.getNextPrincipalRace = function () {
                return cursesFactory.getNextPrincipalRace(userObject.parseObject);
            };

            userObject.getNextSecondaryRace = function () {
                return cursesFactory.getNextSecondaryRace(userObject.parseObject);
            };

            userObject.getRaceWithOutJoin = function (count, callback) {
                //cursesFactory.getRaceWithOutJoin(count, userObject, function (result) { //Add by kazuyou@
                cursesFactory.getUpcomingRaces(count, userObject, function (result) {
                    callback(result)
                })
            };

            userObject.getFeedback = function () {
                return feedbackFactory.getByUser(userObject.parseObject);
            };

            userObject.getRaces = function () {
                return atletaCursaFactory.getByAthlete(userObject)
            };

            userObject.getWeeklyFeedback = function () {
                return feedbackFactory.getWeeklyFeedbackByUser(userObject.parseObject)
            };

            userObject.getLastRaceFeedback = function (callback) {
                atletaCursaFactory.getLastRaceFeedbackByUser(userObject.parseObject, function (result) {
                    callback(result)
                })
            };


            userObject.getCurrentTraining = function (callback) {
                var now = moment().toDate();
                atletaMesocicleFactory.getTrainingForRange(now, now, userObject.parseObject, function (result) {
                    callback(result)
                })
            };

            userObject.getRangeTraining = function (start, end, callback) {
                atletaMesocicleFactory.getTrainingForRange(start, end, userObject.parseObject, function (result) {
                    callback(result)
                })
            };

            userObject.getDisplayMessage = function () {
                var parseObject = Parse.Object.extend("Message");
                var query = new Parse.Query(parseObject);
                return query.get('AhLNVKKuJQ');
            };

            userObject.getRequests = function (callback) {
                if ( userObject.isAthlete() ){
                    requestFactory.getByUser(userObject.parseObject,function(response){
                        callback(response);
                    })

                    /*user.getRequests(function(requests){
                        $scope.env.requests = requests;

                        angular.forEach(request, function(row){
                         if ( row.get('fullrequest')!=undefined ){
                         return;
                         }
                         if (  moment().isAfter( moment( row.get('startDate') ).add(3,'days') ) ){
                         $scope.env.requestAlter = true;
                         }
                         });
                        $scope.$apply();
                    });*/
                }else{
                    requestFactory.getRequestWitOutCoachComment( function(response){
                        callback(response);
                    });
                }

            };

            userObject.getPendingNotifications = function () {
                var promise = new Parse.Promise();
                eventFactory.getUnreadEvents(userObject).then(function(response){
                    promise.resolve(response);
                });
                return promise;

            };

            userObject.getLastEvents = function (limit) {
                console.log(limit)

                var promise = new Parse.Promise();
                eventFactory.getLastEvents(userObject, limit).then(function(response){
                    promise.resolve(response);
                });
                return promise;

            };



            return userObject;
        }


    }
})(angular);

