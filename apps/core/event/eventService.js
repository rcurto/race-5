(function (angular) {
    'use strict';
    angular.module('core').service('eventService', eventService);

    eventService.$inject = ['$filter'];
    function eventService($filter) {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);

            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            //object.attributes.createdAt = moment(object.parseObject.createdAt).format('DD-MM-YYYY');


            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            object.read = function () {
                object.parseObject.set('flgUserView', true);
                object.parseObject.save();
            };
            object.getUrl = function () {

                if ( object.get('type')=='joinRace' || object.get('type')==undefined){
                    return 'race-my'
                }
                if ( object.get('type')=='weeklyFeedback'){
                    return 'feedbacks'
                }if ( object.get('type')=='raceFeedback'){
                    return 'race-my'
                }if ( object.get('type')=='raceTip'){
                    return 'tips'
                }if ( object.get('type')=='request'){
                    return 'requests'
                }
            };
            object.getIntervalMinute = function () {
                return pGetIntervalDate(  moment(object.get("createdAt")) );
            };

            object.getIcon = function () {
                if ( object.get('type')=='joinRace' ){
                    return 'icon-badge'
                }
                return 'icon-badge'
            };
            return object;


            function pGetIntervalDate(date) {
                var currentDate     = moment();
                var pMinute         =  currentDate.diff(date, 'minutes');
                var pHours          =  currentDate.diff(date, 'hours');
                var pDays           =  currentDate.diff(date, 'days');
                if(pDays   > 0)     return (pDays + " " + $filter('translate')("days") );
                if(pHours  > 0)     return (pHours + " " + $filter('translate')("hrs") );
                if(pMinute > 0)     return (pMinute + " " + $filter('translate')("mins") );
                return $filter('translate')("Just Now");
            }
        }
    }
})(angular);

