(function (angular) {
    'use strict';
    angular.module('core').service('countryFactory', countryFactory);

    countryFactory.$inject = ['countryService'];

    function countryFactory(countryService) {
        var cache = [];
        return {
            getById: getById,
            getAll: getAll,
            store: store
        };


        function store(data, callback) {
            var parseObject = Parse.Object.extend("Country");
            var store = new parseObject();
            var i;
            for (i in data) {
                if (i == 'parseObject') {
                    continue;
                }
                store.set(i, data[i]);
            }


            store.save(null, {
                success: function (obj) {
                    callback({success: true});
                },
                error: function (obj, error) {
                    callback({success: false, error: error.message});
                }
            });
        }

        function getById(id, callback) {
            var parseObject = Parse.Object.extend("Country");
            var query = new Parse.Query(parseObject);
            query.include('zones');
            query.get(id, {
                success: function (results) {
                    callback(countryService(results))
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }


        function getAll(callback, wrapper) {
            var promise = new Parse.Promise();

            var parseObject = Parse.Object.extend("Country");
            var query = new Parse.Query(parseObject);
            query.include('zones');
            query.limit(1000);
            query.find({
                success: function (results) {
                    if (wrapper == false) {
                        if (callback){
                            callback(results);
                        }
                        promise.resolve(results);
                        return;
                    }
                    var objs = [];
                    angular.forEach(results, function (row) {
                        var eventObject = countryService(row);
                        objs.push(eventObject);
                    });
                    if (callback) {
                        callback(objs);
                    }
                    promise.resolve(objs);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
            return promise;

        }


    }
})(angular);

