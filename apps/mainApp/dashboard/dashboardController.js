(function () {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$scope', 'userFactory', 'cursesFactory'];

    function dashboardController($scope, userFactory, cursesFactory) {

        $scope.$parent.initPage = init; // start point

        var promises = [];
        $scope.user = null;
        $scope.env = {
            loading: true,
            user: null,
            race: null,
            nextRace: null
        };




        function init() {
            $scope.user = $scope.$parent.user;
            var user = $scope.user;
            var nextRacePromise = user.getNextRace().then(function (result) {
                $scope.env.race = result;
            });
            promises.push(nextRacePromise);

            var categoryPromise = cursesFactory.getObjectByCategory('f7kcko0eDY').then(function (result) {
                $scope.env.nextRace = result;
            });
            
            promises.push(categoryPromise);

            if (user){
                userFactory.saveUserLastLoginDate();
            }
            Parse.Promise.when(promises).then(function () {
                pageLoaded();
            });
        }

        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply();
        }




    }

})();