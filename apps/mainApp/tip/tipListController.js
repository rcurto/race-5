(function () {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('tipListController', tipListController);

    tipListController.$inject = ['$scope', 'atletaCursaFactory', 'userFactory', '$filter','eventFactory'];

    function tipListController($scope, atletaCursaFactory, userFactory, $filter,eventFactory) {

        $scope.$parent.initPage = init; // start point

        var promises = [];
        $scope.user = null;
        $scope.env = {
            loading: true,
            user: null,
            race: null,
            nextRace: null,
            showTip: false,
            races:[],
            racesSource:[],
            filters:{}

        };
        $scope.userFactory = userFactory;
        $scope.config = {
            itemsPerPage: 50,
            fillLastPage: true
        };

        function init() {
            $scope.user = $scope.$parent.user;
            var user = $scope.user;

            if (user.isAthlete()) {
                var nextRacePromise = $scope.user.getNextRace().then(function (result) {
                    $scope.env.race = result;
                    if ( result!=null ){
                        var race = result.get('nomcursa');
                        if (result.get('coachTip')) {
                            $scope.env.showTip = true;
                        }
                        switch (race.get('categoria')) {
                            case('KV'):
                                $scope.stat = {
                                    zone1: user.get('R4') - 5,
                                    zone2: user.get('R4'),
                                    zone3: user.get('R5'),
                                    zone4: user.get('R6') - 2,
                                };
                                break;
                            case('T'):
                                $scope.stat = {
                                    zone1: user.get('R4') - 6,
                                    zone2: user.get('R4') - 2,
                                    zone3: user.get('R4') + 2,
                                    zone4: user.get('R5') + 2,
                                };
                                break;
                            case('MM'):
                                $scope.stat = {
                                    zone1: user.get('R3'),
                                    zone2: user.get('R4') - 5,
                                    zone3: user.get('R4'),
                                    zone4: user.get('R4') + 2,
                                };
                                break;
                            case('UT'):
                                $scope.stat = {
                                    zone1: user.get('R2'),
                                    zone2: user.get('R2') + 5,
                                    zone3: user.get('R3'),
                                    zone4: user.get('R4'),
                                };
                                break;
                            case('10K'):
                                $scope.stat = {
                                    zone1: 10,
                                    zone2: 10,
                                    zone3: 10,
                                    zone4: 10,
                                };
                                break;
                        }
                    }


                    $scope.$apply();
                });
                promises.push(nextRacePromise);
            } else {
                var racesPromise = atletaCursaFactory.getNewByCoach().then(function (posts) {
                    $scope.env.loading = false;
                    $scope.env.races = posts;
                    $scope.env.racesSource = posts;
                });
                promises.push(racesPromise);
            }

            if (promises.length > 0) {
                Parse.Promise.when(promises).then(function () {
                    pageLoaded();
                });
            } else {
                pageLoaded();
            }

        }


        function pageLoaded() {
            $scope.env.loading = false;
            $scope.$apply()
        }

        $scope.saveResult = function (post) {
            var savingFields = {
                coachTip: post.coachTip,
                psychologistAdvice: post.psychologistAdvice,
                nutritionistAdvice: post.nutritionistAdvice,
                beforeCoachComment: post.beforeCoachComment,
                isCoachTipsFull: true
            };
            post.isCoachTipsFull = true;
            post.update(savingFields, function () {
                alertify.success($filter("translate")("Race updated"));


                if (  userFactory.isPremiumPlan(post.user) ){
                    eventFactory.createRaceTips(post.get('user'), post.get('nomcursa'))
                }

            });
        }


        $scope.$watchCollection('env.filters', function () {

            var data = null;
            var trainingPlan = $scope.env.filters.trainingPlan ? $scope.env.filters.trainingPlan : null;

            var userName = $scope.env.filters.userName!=undefined ? $scope.env.filters.userName.toLowerCase() : null;
            if ($scope.env.filters.printData) {
                data = $filter("date")($scope.env.filters.printData, 'dd-MM-yyyy');
            }

            $scope.env.races = $scope.env.racesSource.filter(function (value) {

                if (userName != null && value.userName && value.userName.toLowerCase().indexOf(userName) == -1) {
                    return false;
                }

                if (trainingPlan != null && value.trainingPlan != trainingPlan) {
                    return false;
                }

                if (data != null && value.printData.indexOf(data) == -1) {
                    return false;
                }


                return true;
            })

        });


    }

})();