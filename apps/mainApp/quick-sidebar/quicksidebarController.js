/*
ClassName	:	quicksidebarController
Maker		:	Add by kazuyou@
Date		:	
Description : Controll real time chatting between private user with PubNub.
*/
(function() {

    'use strict';

    angular
        .module('MetronicApp')
        .controller('quicksidebarController', quicksidebarController);
	
    quicksidebarController.$inject=['$rootScope', '$scope', '$location', 'PubNub', 'userFactory','atletaCursaFactory', 'cursesFactory', 'feedbackFactory', 'eventFactory'];

    function quicksidebarController($rootScope, $scope, $location, PubNub, userFactory, atletaCursaFactory, cursesFactory, feedbackFactory, eventFactory) {

		  $scope.userId			= localStorage.getItem("username");// + Math.round(Math.random() * 1000);
		  $scope._theChannel	= "ramonchnnel";
		  $scope._authsplitkey	= "#@kazuyou@#";
		  $scope._msgsplitkey	= "#@kazuyoukey@#";
		  $scope._usrPhotSrc	= "";
		  $scope.messages		= new Array();
		  
		$scope.env = {
			/*******************PUBNUB***************************/
			aryJoinUsers				: [],
			aryTimeoutUser				: null,
			aryLeaveUser				: null,
			_chatToUser					: null,
			_onlineAllUserCount			: 0,
			_chatToUserid				: null,
			_chatToUserStatus			: 'online',
			_noticeChatMsg				: "",
			_isHistory					: true,
			_channelList				: "ramonchnnel#@#",
			_channelMsgList				: {},
			/*******************PUBNUB***************************/
            user						: null,
			staffUserList				: null,
			athleteUserList				: null,
            nextPrincipalRace			: null,
            nextSecondaryRace			: null,
            lastRaceResult:[],
            lastNewRace:[],
            topRecords:[],
            topRaces:[],
            feedback:[],
            topRunningShoes				: [],
            feedbackAlter				: false,
            missingRaceFeedback			: null,
/*            
            weeklyFeedback				: undefined,
            lastRaceFeedback			: undefined,
            nextRaceCoachTip			: undefined,
            requestCoachComment			: undefined,
            nextTrainingNotify			: false,
*/
			pendingNotificationList		: [],            
            itvNextTraining				: "Just Now",
            iconWeeklyFeedback 			: "icon-speech",
            iconRaceFeedback 			: "icon-trophy",
            iconCoachTip	 			: "fa fa-area-chart",
            iconNextTraning	 			: "icon-notebook",
            iconRequest		 			: "icon-call-out",
			_CurrentClsChatStatus		: 'badge badge-success',
			raceforcoachtips			: undefined,
            events						: []
        };

       userFactory.auth( function(user){
            $scope.env.user = user;

            $scope.getPendingNotifications = function(){
				userFactory.getPendingNotifications(function(pendingNotificationList){

					if(user.get("startTrain") && userFactory.getChkNotificationForNextTraning(user.get("startTrain")) == true){
						
						$scope.env.nextTrainingNotify 	= true;
						var pStartTrain 				= new Date(user.get("startTrain"));

						pStartTrain.setDate(pStartTrain.getDate() + 13);

						var pTimeStamp			= pStartTrain.getTime();
						var pNextTraningInterval = userFactory.pGetIntervalDate(pTimeStamp);

						pendingNotificationList.push({
							objectId 		: "",
							noticeMsg 		: "Remember to write your comments for the next training plan",
							intervalMinute 	: pNextTraningInterval,
							coachComment   	: "x`",
							timestampe 		: pTimeStamp,
							tblName 		: "",
							icon 			: "icon-notebook",
							noticeTitle		: ""
						});
					}

					$scope.env.pendingNotificationList = pendingNotificationList;

					$scope.$apply();
				});
			};
			/*
			if(user.isAthlete() == true){
            	$scope.getPendingNotifications(); 
            }
			
			$scope.refreshPendingNotification = function(){
				$scope.getPendingNotifications();
			};
			*/
        });
		
		userFactory.getQuickSideBarUsers( function(userList){

			$scope.env.athleteUserList	= userList.sidebarAthleteArray;
			$scope.env.staffUserList	= userList.sidebarStaffArray;

			$scope.$apply();

			QuickSidebar.init();

			userFactory.getNewChatsMessages(userList, function(newChatMsgUserList){
				$scope.env.athleteUserList	= newChatMsgUserList.sidebarAthleteArray;
				$scope.env.staffUserList	= newChatMsgUserList.sidebarStaffArray;
				$scope.$apply();
			});

			/*************************PUBNUB*****************************/
			// set up initial channel memberships
			// pre-populate any existing messages (just an AngularJS scope object)

			  if (!$rootScope.initialized) {
				// Initialize the PubNub service
				PubNub.init({
				  subscribe_key	: 'sub-c-779b191a-e1f0-11e5-ad87-0619f8945a4f',
				  publish_key	: 'pub-c-156e9311-9096-43af-b5d5-bf9255c3d5e9',
				  uuid:$scope.userId
				});
				$rootScope.initialized = true;
			  }

			  // Create a function to subscribe to a channel
			$scope.subscribe = function(theChannel) {
				$scope._theChannel = theChannel;
				
				if($scope.env._channelList.indexOf(theChannel+"#@#") == -1 || $scope._theChannel == "ramonchnnel"){
					$scope.env._channelList = $scope.env._channelList + theChannel+"#@#";
				}else{
					return;
				}
				PubNub.ngSubscribe({
					channel	: theChannel,
					presence: function(data) {
						// get notified when people join
						if(data[0].action == "join") {
						  if(data[1].uuids){
							$scope.env.aryJoinUsers = data[1].uuids;
						  }
						  else{
							$scope.env.aryJoinUsers = [data[0].uuid];
						   }
							
						   $scope.setJoinChatUserStatus();
						   $scope.setCountOnlineStatus();

						}else if(data[0].action == "leave") {
							 $scope.env.aryLeaveUser = data[0].uuid;

							 $scope.setLeaveUserStatus();
							 $scope.setCountOnlineStatus();
						}else if(data[0].action == "timeout"){
							 $scope.env.aryTimeoutUser = data[0].uuid;

							$scope.setTimeOutChatUserStatus();
							$scope.setCountOnlineStatus();
						}
					  },
					 disconnect:function(data){ console.log(data)},
					 uuid			: $scope.userId
				});
				// Register for message events
				$rootScope.$on(PubNub.ngMsgEv(theChannel), function(ngEvent, payload) {
				  $scope.$apply(function() {
					if(payload.message){
						var message		= payload.message;

						var tmpAry		= new Array();

						if(message.split && message.split($scope._msgsplitkey).length > 1){
							$scope.messages = [];
							tmpAry = message.split($scope._msgsplitkey);
						}else{
							tmpAry.push(message);
						}

						for(var i= 0; i< tmpAry.length; i++){
							
							var msg = tmpAry[i];

							if(msg.split){
								var priCurUserId= new Parse.User.current().id;
								var aryMsg		= msg.split($scope._authsplitkey);
								var userid		= "";
								var imgsrc		= "";
								var newMsgCt	= 0;
								var fromUser	= "";
								var toUser		= "";
								var regdate		= "";

								if(aryMsg.length > 2 && aryMsg[0])
									msg = aryMsg[0];

								if(aryMsg.length > 2 && aryMsg[1])
									userid = aryMsg[1];
							
								if(aryMsg.length > 2 && aryMsg[2])
									imgsrc = aryMsg[2];

								if(aryMsg.length > 3 && aryMsg[3])
									regdate = aryMsg[3];
								
								if(aryMsg.length > 4 && aryMsg[4])
									toUser = aryMsg[4];//ToUser

								if(aryMsg.length > 5 && aryMsg[5])
									fromUser = aryMsg[5];//FromUser

								if(aryMsg.length > 6 && aryMsg[6])
									newMsgCt = aryMsg[6];//NewMsgCt
								
								if(priCurUserId == toUser){
									for(var i= 0; i< $scope.env.staffUserList.length; i++){
					
										var tmp = $scope.env.staffUserList[i];

										if(tmp.userid == fromUser){
											$scope.env.staffUserList[i].onlineStatusUserCount = newMsgCt;
										}
									}
									for(var j= 0; j< $scope.env.athleteUserList.length; j++){
									
										var tmp = $scope.env.athleteUserList[j];

										if(tmp.userid == fromUser){
											$scope.env.athleteUserList[j].onlineStatusUserCount = newMsgCt;
										}
									}
								}
								$scope.messages.push({msg:msg, userid: userid, datetime: regdate, imgSrc: imgsrc});
							}
						}
					}
				  });

					var wrapper = $('.page-quick-sidebar-wrapper');
					var wrapperChat = wrapper.find('.page-quick-sidebar-chat');
					var chatContainer = wrapperChat.find(".page-quick-sidebar-chat-user-messages");
					
					var getLastPostPos = function() {
						var height = 0;
						chatContainer.find(".post").each(function() {
							height = height + $(this).outerHeight();
						});

						return height;
					};
					chatContainer.slimScroll({scrollTo: getLastPostPos()});
				});
			  };
			  
			  // Create a function to unsubscribe from a channel
			  $scope.unsubscribe = function(theChannel, $timeout) {
				// Unsubscribe from the channel via PubNub
				PubNub.ngUnsubscribe({ channel: theChannel,
										callback: function(m){
									                userFactory.logout();
													$timeout(function(){
														window.location.href = 'index.html'
													},100)
										}});
			  };
			  // Create a publish() function in the scope
			  $scope.publish = function(msg, flag) {

				PubNub.ngPublish({
				  channel: $scope._theChannel,
				  uuid:$scope.userId,
				  message: msg
				});

			  };
			  // Create a subscribe/unsubscribe click handler
			  $scope.updateSubscription = function(theChannel) {
				 $scope._theChannel = theChannel;
				 $scope.subscribe(theChannel);
			  };
			  
			  $scope.channelHistory = function(theChannel) {
				// Unsubscribe from the channel via PubNub
				PubNub.ngHistory(
					{ 
						channel	: theChannel,
//						start	: '13406746780720711',
//						reverse	: true,
						count	: 300,
						callback: function(hisdata){

						}
					});
			  };
			  // Set up the initial channel subscriptions
			  $scope.updateSubscription($scope._theChannel);
			/**************************PUBNUB***************************/
        });

		$scope.setTimeOutChatUserStatus = function(){
			
			if($scope.env.staffUserList && $scope.env.staffUserList.length > 0){

				for(var i= 0; i< $scope.env.staffUserList.length; i++){
				
					var tmp = $scope.env.staffUserList[i];

					if(tmp.username == $scope.env.aryTimeoutUser){

						if(tmp.username == $scope.env._chatToUser) $scope.setViewChattingUserStatus("offline");

						$scope.env.staffUserList[i].clsChatStatus		= "badge badge-danger";
						$scope.env.staffUserList[i].actionChatStatus	= "offline";
					}
				}
			}

			if($scope.env.athleteUserList && $scope.env.athleteUserList.length > 0){

				for(var j= 0; j< $scope.env.athleteUserList.length; j++){
				
					var tmp = $scope.env.athleteUserList[j];

					if(tmp.username == $scope.env.aryTimeoutUser){

						if(tmp.username == $scope.env._chatToUser) $scope.setViewChattingUserStatus("offline");

						$scope.env.athleteUserList[j].clsChatStatus		= "badge badge-danger";
						$scope.env.athleteUserList[j].actionChatStatus	= "offline";
					}
				}
			}
			$scope.$apply()
		};
		$scope.setLeaveUserStatus = function(){
			
			if($scope.env.staffUserList && $scope.env.staffUserList.length > 0){

				for(var i= 0; i< $scope.env.staffUserList.length; i++){
				
					var tmp = $scope.env.staffUserList[i];

					if(tmp.username == $scope.env.aryLeaveUser){
						
						if(tmp.username == $scope.env._chatToUser) $scope.setViewChattingUserStatus("offline");

						$scope.env.staffUserList[i].clsChatStatus		= "badge badge-danger";
						$scope.env.staffUserList[i].actionChatStatus	= "offline";
					}
				}
			}

			if($scope.env.athleteUserList && $scope.env.athleteUserList.length > 0){

				for(var j= 0; j< $scope.env.athleteUserList.length; j++){
				
					var tmp = $scope.env.athleteUserList[j];

					if(tmp.username == $scope.env.aryLeaveUser){
						
						if(tmp.username == $scope.env._chatToUser) $scope.setViewChattingUserStatus("offline");

						$scope.env.athleteUserList[j].clsChatStatus		= "badge badge-danger";
						$scope.env.athleteUserList[j].actionChatStatus	= "offline";
					}
				}
			}

			$scope.$apply()
		};

		$scope.setJoinChatUserStatus = function(){

			var options = {year: "numeric", month: "short", day: "numeric", hour: "2-digit", minute: "2-digit", second : "2-digit"};
			var currentDate = new Date().toLocaleTimeString("en-us", options);

			var tmpAry = {};
			var loginIndex = 0;

			tmpAry = $scope.env.staffUserList;

			if($scope.env.staffUserList && $scope.env.staffUserList.length > 0){

				for(var i= 0; i< $scope.env.staffUserList.length; i++){
				
					if($scope.env.aryJoinUsers  && $scope.env.aryJoinUsers.length > 0){
						for(var j= 0; j< $scope.env.aryJoinUsers.length; j++){

							var tmp = $scope.env.staffUserList[i];

							if(tmp.username == $scope.env.aryJoinUsers[j]){

								if(i > 0){
									tmp.lastLoginDate = currentDate;
									tmpAry.splice(i, 1);
									tmpAry.unshift(tmp);
								}

								if(tmp.username == $scope.env._chatToUser) $scope.setViewChattingUserStatus("online");

								tmpAry[loginIndex].clsChatStatus		= "badge badge-success";
								tmpAry[loginIndex].actionChatStatus		= "online";

								loginIndex++;
							}
						}
					}
				}
			}
			$scope.env.staffUserList = tmpAry;

			tmpAry = $scope.env.athleteUserList;
			loginIndex = 0;

			  if($scope.env.athleteUserList && $scope.env.athleteUserList.length > 0){

				for(var k= 0; k< $scope.env.athleteUserList.length; k++){
				
					if($scope.env.aryJoinUsers  && $scope.env.aryJoinUsers.length > 0){
						for(var h= 0; h< $scope.env.aryJoinUsers.length; h++){

							var tmp1 = $scope.env.athleteUserList[k];

							if(tmp1.username == $scope.env.aryJoinUsers[h]){
								
								if(k > 0){
									tmp1.lastLoginDate = currentDate;
									tmpAry.splice(k, 1);
									tmpAry.unshift(tmp1);
								}

								if(tmp1.username == $scope.env._chatToUser) $scope.setViewChattingUserStatus("online");

								tmpAry[loginIndex].clsChatStatus		= "badge badge-success";
								tmpAry[loginIndex].actionChatStatus		= "online";

								loginIndex++;
							}
						}
					}
				}
			}

			$scope.env.athleteUserList = tmpAry;
	
			$scope.$apply()
		}

		$scope.getCountOnlineStatus = function(flag){

			var statusCt = 0;

			for(var i= 0; i< $scope.env.staffUserList.length; i++){
			
				var tmp = $scope.env.staffUserList[i];

				if($scope.env.staffUserList[i].actionChatStatus == flag){
					statusCt++;
				}
			}

			for(var j= 0; j< $scope.env.athleteUserList.length; j++){
			
				var tmp = $scope.env.athleteUserList[j];

				if($scope.env.athleteUserList[j].actionChatStatus == flag){
					statusCt++;
				}
			}

			return statusCt;
		};
		$scope.setCountOnlineStatus = function(){

			var onlineCt	= $scope.getCountOnlineStatus('online');

			$scope.env._onlineAllUserCount		= $scope.getCountOnlineStatus('online');
			
			if(onlineCt > 0)
				$scope.env._onlineClsAllUserCount	= 'badge badge-success';
			else
				$scope.env._onlineClsAllUserCount	= 'badge badge-danger';

			$scope.$apply();
		}

		$scope.saveSideBarChatMessage = function(msg){
		
			var fromUserId	= new Parse.User.current().id;
			var toUserId	= $scope.env._chatToUserid;

			var sidebarObject		= Parse.Object.extend("ChatsMessages");
            var objSidebarChatMsg	= new sidebarObject();

			var regdate			= new Date();

			 if(fromUserId && toUserId){
				
				 objSidebarChatMsg.set("fromUser",		fromUserId);
				 objSidebarChatMsg.set("toUser",		toUserId);
				 objSidebarChatMsg.set("message",		msg);
				 objSidebarChatMsg.set("regdate",		regdate);
				 objSidebarChatMsg.set("chatStatus",	"new");

				 var sidebarObject = angular.copy(objSidebarChatMsg.attributes);
				
				sidebarObject.parseObject = angular.copy(objSidebarChatMsg);
				sidebarObject.attributes  = angular.copy(objSidebarChatMsg.attributes);

				sidebarObject.parseObject.save();

			 }

			 $scope.updateSideBarChatStatus();
		}
		
		$scope.updateSideBarChatStatus = function(){
		
			var toUserId	= new Parse.User.current().id;
			var fromUserId  = $scope.env._chatToUserid;

			var objSidebarChatMsg = Parse.Object.extend("ChatsMessages");
			var query = new Parse.Query(objSidebarChatMsg);

			query.equalTo("toUser",		toUserId);
			query.equalTo("fromUser",	fromUserId);
			query.equalTo("chatStatus", "new");

			query.find({
                success: function(results) {
					for (var i = 0, len = results.length; i < len; i++) {
							var result    = results[i];
							
							result.set("chatStatus", "old");

						    result.save(null, {
								success:function(obj) {
									console.log("Successfully saved");
								},
								error:function(err) { 
									console.log("Not successfully saved");
								}
	
							});              
					}
				},
				error: function(error) {
					console.log("Error: " + error.code + " " + error.message);
				} 
			});

			$scope.updateZeroMsgCount();
		}

		$scope.setToUserid = function(toUserName){

			for(var i= 0; i< $scope.env.staffUserList.length; i++){
			
				var tmp = $scope.env.staffUserList[i];

				if(tmp.username == toUserName){
					$scope.env._chatToUserid = tmp.userid;
				}
			}
			for(var j= 0; j< $scope.env.athleteUserList.length; j++){
			
				var tmp = $scope.env.athleteUserList[j];

				if(tmp.username == toUserName){
					$scope.env._chatToUserid = tmp.userid;
				}
			}
		}

		$scope.setNewMsgCount = function(){
			
			var toUserId  = new Parse.User.current().id;

			for(var i= 0; i< $scope.env.staffUserList.length; i++){
			
				var tmp = $scope.env.staffUserList[i];

				$scope.getNewMsgCount(tmp.userid, toUserId, 'staff', i);
			}
			for(var j= 0; j< $scope.env.athleteUserList.length; j++){
			
				var tmp = $scope.env.athleteUserList[j];

				$scope.getNewMsgCount(tmp.userid, toUserId, 'athlete', j);
			}

			$scope.$apply()
		}
		$scope.getNewMsgCount = function(fromUserId, toUserId, userflag, usernumber){

			
			var objSidebarChatMsg = Parse.Object.extend("ChatsMessages");
			var query = new Parse.Query(objSidebarChatMsg);

			query.equalTo("toUser",		""+toUserId);//
			query.equalTo("fromUser",	""+fromUserId);//
			query.equalTo("chatStatus", "new");
			
			var newMsgCt = 0;

			query.find({
                success: function(results) {

					if(results){
						newMsgCt = results.length;

						if(newMsgCt > 0){
							if(userflag == 'staff'){
								$scope.env.staffUserList[usernumber].onlineStatusUserCount = newMsgCt;
							}
							else{
								$scope.env.athleteUserList[usernumber].onlineStatusUserCount = newMsgCt;
							}
						}
					}
				}
			})
		}
		$scope.getOneUserNewMsgCount = function(msg){
			
			var toUserId	= $scope.env._chatToUserid;
			var fromUserId  = new Parse.User.current().id;

			$scope.env._isHistory = false;

			var objSidebarChatMsg = Parse.Object.extend("ChatsMessages");
			var query = new Parse.Query(objSidebarChatMsg);

			query.equalTo("toUser",		""+toUserId);//
			query.equalTo("fromUser",	""+fromUserId);//
			query.equalTo("chatStatus", "new");
			
			var newMsgCt = 0;

			query.find({
                success: function(results) {

					if(results){
						newMsgCt = results.length;

						var options = {year: "numeric", month: "short", day: "numeric", hour: "2-digit", minute: "2-digit", second : "2-digit"};
						var regdate = new Date().toLocaleTimeString("en-us", options);

						msg =	msg + $scope._authsplitkey + $scope.userId + $scope._authsplitkey + $scope._usrPhotSrc + 
								$scope._authsplitkey + regdate + $scope._authsplitkey + toUserId + $scope._authsplitkey + fromUserId + $scope._authsplitkey + 
								newMsgCt + $scope._authsplitkey;

						$scope.publish(msg);

					}
				}
			})
		}
		$scope.updateZeroMsgCount = function(){

			var fromUserId  = $scope.env._chatToUserid;

			for(var i= 0; i< $scope.env.staffUserList.length; i++){
			
				var tmp = $scope.env.staffUserList[i];

				if(tmp.userid == fromUserId){
					$scope.env.staffUserList[i].onlineStatusUserCount = 0;
				}
			}
			for(var j= 0; j< $scope.env.athleteUserList.length; j++){
			
				var tmp = $scope.env.athleteUserList[j];

				if(tmp.userid == fromUserId){
					$scope.env.athleteUserList[j].onlineStatusUserCount = 0;
				}
			}
			$scope.$apply();
		}
		$scope.updateAthletePendingNotice = function(objectId, tblName){

			var updateClass = Parse.Object.extend(tblName);
			var query 		= new Parse.Query(updateClass);

			query.equalTo("objectId",		objectId);

			query.first({
                success: function(result) {

					result.set("flgUserView", true);

					result.save(null, {
						success:function(obj) {
							console.log("Successfully Athlete Comment View saved");
						},
						error:function(err) { 
							console.log("Not successfully Athlete Comment View saved");
						}
					});    
				},
				error: function(error) {
					alert("Error: " + error.code + " " + error.message);
				} 
			});
		}
		$scope.setViewChattingUserStatus = function(priChatToUser){

			if(priChatToUser == "online")
				$scope.env._CurrentClsChatStatus = 'badge badge-success';
			else{
				priChatToUser == "offline";
				$scope.env._CurrentClsChatStatus = 'badge badge-danger';
			}

			$scope.env._noticeChatMsg = $scope.env._chatToUser + " is " + priChatToUser;

			$scope.$apply();
		}

		$scope.getOldMesseges = function(){
			
		}
		$scope.setHistory = function(){
//_channelMsgList
//console.log($scope._theChannel);
			$scope.messages = [];
			if(!$scope.env._channelMsgList || !$scope.env._channelMsgList[$scope._theChannel]){
					
					// Subscribe to the channel via PubNub
					var objSidebarChatMsg = Parse.Object.extend("ChatsMessages");
					var query1 = new Parse.Query(objSidebarChatMsg);


					var toUserId	= new Parse.User.current().id;
					var fromUserId	= $scope.env._chatToUserid;
					var toUserPhoto	=  $scope._usrPhotSrc;
					var fromUserPhoto	= "";
					var toUserName	= $scope.userId;
					var fromUserName = "";

					for(var i= 0; i< $scope.env.staffUserList.length; i++){

						var tmp = $scope.env.staffUserList[i];

						if($scope.env.staffUserList[i].userid == toUserId){
							toUserPhoto = $scope.env.staffUserList[i].userphoto;
							toUserName =  $scope.env.staffUserList[i].username;
						}
						if($scope.env.staffUserList[i].userid == fromUserId){
							fromUserPhoto = $scope.env.staffUserList[i].userphoto;
							fromUserName    =  $scope.env.staffUserList[i].username;
						}
					}

					for(var j= 0; j< $scope.env.athleteUserList.length; j++){
					
						var tmp = $scope.env.athleteUserList[j];

						if($scope.env.athleteUserList[j].userid == toUserId){
							toUserPhoto = $scope.env.athleteUserList[j].userphoto;
							toUserName =  $scope.env.athleteUserList[j].username;
						}
						if($scope.env.athleteUserList[j].userid == fromUserId){
							fromUserPhoto = $scope.env.athleteUserList[j].userphoto;
							fromUserName    =  $scope.env.athleteUserList[j].username;
						}
					}

					query1.equalTo("toUser",	""+toUserId);//
					query1.equalTo("fromUser",	""+fromUserId);//

					var query2 = new Parse.Query(objSidebarChatMsg);

					query2.equalTo("toUser",	""+fromUserId);//
					query2.equalTo("fromUser",	""+toUserId);//

					var mainQuery = Parse.Query.or(query1, query2);
					
					mainQuery.ascending("regdate");

					mainQuery.find({
						success: function(results) {
							var tmpAry      = new Array();
							var allMsg		= "";

							for (var i = 0, len = results.length; i < len; i++) {
								
								var result    = results[i];
								
								var msg			= result.get("message");
								var priuserid	= result.get("fromUser");
								var datetime	= result.get("regdate");
								var tmpPhoto	= "";
								var tmpUserName = "";

								if(priuserid == toUserId){ 
									tmpPhoto = toUserPhoto;
									tmpUserName = toUserName;
								}
								if(priuserid == fromUserId) {
									tmpPhoto = fromUserPhoto;
									tmpUserName = fromUserName;
								}
								$scope.messages.push({msg:msg, userid: tmpUserName, datetime: datetime, imgSrc: tmpPhoto});
							}

							$scope.env._channelMsgList[$scope._theChannel] = $scope.messages;

							$scope.$apply();
						},
						error: function(error) {
							alert("Error: " + error.code + " " + error.message);
						} 
					});
			}else{
				if($scope.env._channelMsgList[$scope._theChannel])
					$scope.messages = $scope.env._channelMsgList[$scope._theChannel];
					
				$scope.$apply();
			}

				
		}

    }
})();